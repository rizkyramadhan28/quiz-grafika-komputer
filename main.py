# Nama  : Rizky Ramadhan
# Kelas : S1IF-05-MM4
# NIM   : 17102044


from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from random import randrange
from OpenGLContext import testingcontext
import math


# Fungsi untuk mendapatkan warna RGBA
# yang sesuai dengan OpenGL.

def get_rgb_value(color_code):
    return color_code / 255.0


# Return list dari kumpulan vertex dengan nilai random.

def get_list_of_random_vertex(total_vertex, min_value, max_value):

    list_of_vertex = []

    for i in range(total_vertex):
        list_of_vertex.append(
            {"x": randrange(min_value, max_value), "y": randrange(min_value, max_value)})

    return list_of_vertex


# Default value untuk koordinat object 2D

koordinat_x = 0
koordinat_y = 0


# Default value untuk total vertex dan nilai minimal
# atau maksimal untuk vertex itu sendiri.

total_vertex = 10
min_vertex_value = -30
max_vertex_value = 30


# Warna default untuk object 2D.


color_red_value = 1.0
color_green_value = 1.0
color_blue_value = 1.0

current_object_color = "White"


# Warna ini didapat dari https://flatuicolors.com/palette/us
# Angka pada koleksi warna ini berguna sebaga id
# dan mewakili jumlah vertex yang ada.

list_of_object_color = {
    10: {"red": get_rgb_value(0.0), "green": get_rgb_value(184.0),
         "blue": get_rgb_value(148.0), "alpha": 1.0, "name": "Mint Leaf"},

    11: {"red": get_rgb_value(253.0), "green": get_rgb_value(203.0),
         "blue": get_rgb_value(110.0), "alpha": 1.0, "name": "Bright Yarrow"},

    12: {"red": get_rgb_value(0.0), "green": get_rgb_value(206.0),
         "blue": get_rgb_value(201.0), "alpha": 1.0, "name": "Robin's Egg Blue"},

    13: {"red": get_rgb_value(225.0), "green": get_rgb_value(112.0),
         "blue": get_rgb_value(85.0), "alpha": 1.0, "name": "Orangeville"},

    14: {"red": get_rgb_value(9.0), "green": get_rgb_value(132.0),
         "blue": get_rgb_value(227.0), "alpha": 1.0, "name": "Electron Blue"},

    15: {"red": get_rgb_value(214.0), "green": get_rgb_value(48.0),
         "blue": get_rgb_value(49.0), "alpha": 1.0, "name": "Chi-Gong"},

    16: {"red": get_rgb_value(108.0), "green": get_rgb_value(92.0),
         "blue": get_rgb_value(231.0), "alpha": 1.0, "name": "Exodus Fruit"},

    17: {"red": get_rgb_value(232.0), "green": get_rgb_value(67.0),
         "blue": get_rgb_value(147.0), "alpha": 1.0, "name": "Prunus Avium"},

    18: {"red": get_rgb_value(178.0), "green": get_rgb_value(190.0),
         "blue": get_rgb_value(195.0), "alpha": 1.0, "name": "Soothing Breeze"},

    19: {"red": get_rgb_value(45.0), "green": get_rgb_value(52.0),
         "blue": get_rgb_value(54.0), "alpha": 1.0, "name": "Dracula Orchid"}
}


# List untuk background color

list_of_background_color = {
    "quadrant_1": {"red": get_rgb_value(116.0), "green": get_rgb_value(185.0),
                   "blue": get_rgb_value(255.0), "alpha": 1.0, "name": "Green Darner Tail"},

    "quadrant_2": {"red": get_rgb_value(255.0), "green": get_rgb_value(118.0),
                   "blue": get_rgb_value(117.0), "alpha": 1.0, "name": "Pink Glamour"},

    "quadrant_3": {"red": get_rgb_value(162.0), "green": get_rgb_value(155.0),
                   "blue": get_rgb_value(254.0), "alpha": 1.0, "name": "Shy Moment"},

    "quadrant_4": {"red": get_rgb_value(99.0), "green": get_rgb_value(110.0),
                   "blue": get_rgb_value(114.0), "alpha": 1.0, "name": "American River"}
}

# Kumpulan sudut default untuk membuat object 2D

list_of_vertex = get_list_of_random_vertex(
    total_vertex, min_vertex_value, max_vertex_value)


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-100.0, 100.0, -100.0, 100.0)
    glPointSize(10)


# Membuat teks dengan menggunakan bitmap

def drawBitmapText(string, x, y, z):
    glRasterPos3f(x, y, z)
    for c in string:
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ord(c))


# Menggambar object 2D

def draw_random_2d_object(list_of_vertex):

    glColor3f(color_red_value, color_green_value, color_blue_value)

    glBegin(GL_POLYGON)

    for sudut in list_of_vertex:
        glVertex2f(sudut.get("x"), sudut.get("y"))
        # print(f"x: {sudut.get('x')}, y: {sudut.get('y')}")

    glEnd()


# Interaksi user dengan menggunakan mouse
# untuk mengubah warna.

def get_mouse_user_input(button, state, x, y):

    global list_of_vertex
    global total_vertex
    global color_red_value
    global color_green_value
    global color_blue_value
    global current_object_color
    global koordinat_x
    global koordinat_y

    # Jika user melakukan klik kanan atau klik kiri

    if (button == GLUT_RIGHT_BUTTON and state == GLUT_DOWN) or (button == GLUT_LEFT_BUTTON and state == GLUT_DOWN):

        # Mengubah koordinat kembali menjadi 0

        koordinat_x = 0
        koordinat_y = 0

        # Mendapatkan warna yang sesuai dengan total vertex
        # lalu ubah warna default object 2D.

        current_collection_color = list_of_object_color.get(total_vertex)

        color_red_value = current_collection_color.get("red")
        color_green_value = current_collection_color.get("green")
        color_blue_value = current_collection_color.get("blue")

        current_object_color = current_collection_color.get("name")

        # Menambah total vertex setiap kali klik terjadi
        # dan mendapatkan list vertex baru.

        total_vertex += 1

        list_of_vertex = get_list_of_random_vertex(
            total_vertex, min_vertex_value, max_vertex_value)

        if total_vertex > 19:
            total_vertex = 10

        draw_random_2d_object(list_of_vertex)
        glutPostRedisplay()


# Fungsi untuk melakukan translasi.
# Rumus:
# x' = x + tx
# y' = y + ty

def translasi_2d_object(list_of_vertex, koordinat_type, move_point):

    for vertex in list_of_vertex:
        vertex[koordinat_type] += move_point


# Fungsi untuk melakukan translasi berdasarkan
# input dari keyboard user.

def get_user_special_keyboard_input(key, x, y):

    global list_of_vertex
    global koordinat_x
    global koordinat_y

    # Menggerakkan object 2D

    if key == GLUT_KEY_UP:
        koordinat_y += 10
        translasi_2d_object(list_of_vertex, "y", 10)

    elif key == GLUT_KEY_DOWN:
        koordinat_y -= 10
        translasi_2d_object(list_of_vertex, "y", -10)

    elif key == GLUT_KEY_LEFT:
        koordinat_x -= 10
        translasi_2d_object(list_of_vertex, "x", -10)

    elif key == GLUT_KEY_RIGHT:
        koordinat_x += 10
        translasi_2d_object(list_of_vertex, "x", 10)

    # Mengganti warna background

    # Quadrant 1

    if koordinat_x > 0 and koordinat_y > 0:
        current_background_color = list_of_background_color.get("quadrant_1")

        glClearColor(current_background_color.get("red"),
                     current_background_color.get("green"),
                     current_background_color.get("blue"),
                     current_background_color.get("alpha"))

    # Quadrant 2

    if koordinat_x < 0 and koordinat_y > 0:
        current_background_color = list_of_background_color.get("quadrant_2")

        glClearColor(current_background_color.get("red"),
                     current_background_color.get("green"),
                     current_background_color.get("blue"),
                     current_background_color.get("alpha"))

    # Quadrant 3

    if koordinat_x < 0 and koordinat_y < 0:
        current_background_color = list_of_background_color.get("quadrant_3")

        glClearColor(current_background_color.get("red"),
                     current_background_color.get("green"),
                     current_background_color.get("blue"),
                     current_background_color.get("alpha"))

    # Quadrant 4

    if koordinat_x > 0 and koordinat_y < 0:
        current_background_color = list_of_background_color.get("quadrant_4")

        glClearColor(current_background_color.get("red"),
                     current_background_color.get("green"),
                     current_background_color.get("blue"),
                     current_background_color.get("alpha"))

    glutPostRedisplay()


# Fungsi untuk melakukan scalling.
# Rumus:
# x' = x * sx
# y' = y * sy

def scalling_2d_object(list_of_vertex, scalling_type, koordinat_type, scale_point):

    if scalling_type == "zoom-in":
        for vertex in list_of_vertex:
            vertex[koordinat_type] *= scale_point

    elif scalling_type == "zoom-out":
        for vertex in list_of_vertex:
            vertex[koordinat_type] /= scale_point


# Fungsi untuk melakukan rotasi pada object
# Rumus:
# x = x * cos A - y * sin A
# y = x * sin A + y * cos A

def rotate_2d_object(list_of_vertex, angle_point):

    for vertex in list_of_vertex:
        vertex_x = vertex["x"]
        vertex_y = vertex["y"]

        new_x_angle = vertex_x * \
            math.cos(angle_point) - vertex_y * math.sin(angle_point)

        new_y_angle = vertex_x * \
            math.sin(angle_point) + \
            vertex_y * math.cos(angle_point)

        vertex["x"] = new_x_angle
        vertex["y"] = new_y_angle


# Fungsi untuk scalling dan rotasi

def get_user_keyboard_func(key, x, y):

    decoded_key = key.decode("utf-8")

    global list_of_vertex
    global koordinat_x
    global koordinat_y

    # Commands:

    # 1 => Zoom in
    # 2 => Zoom out
    # 3 => Rotate

    if decoded_key == "1":
        scalling_2d_object(list_of_vertex, "zoom-in", "x", 2)
        scalling_2d_object(list_of_vertex, "zoom-in", "y", 2)

    elif decoded_key == "2":
        scalling_2d_object(list_of_vertex, "zoom-out", "x", 2)
        scalling_2d_object(list_of_vertex, "zoom-out", "y", 2)

    elif decoded_key == "3":
        rotate_2d_object(list_of_vertex, 5)

    koordinat_x = 0
    koordinat_y = 0

    glutPostRedisplay()


def display():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1.0, 1.0, 1.0)

    # Menampilkan status warna

    drawBitmapText(f"Commands (keyboard): ", -95, -20, 0)

    drawBitmapText(f"Scalling - zoom in: 1", -95, -40, 0)
    drawBitmapText(f"Scalling - zoom out: 2", -95, -50, 0)
    drawBitmapText(f"Rotate: 3", -95, -60, 0)

    drawBitmapText(f"Color: {current_object_color}", -95, -80, 0)
    drawBitmapText(f"Total vertex: {total_vertex}", -95, -90, 0)

    # Menggambar garis pembatas untuk x dan y.

    glBegin(GL_LINES)

    glVertex(-100, 0)
    glVertex(100, 0)

    glVertex(0, 100)
    glVertex(0, -100)

    glEnd()

    # Menggambar bentuk default dari object 2D.

    draw_random_2d_object(list_of_vertex)

    glFlush()
    glutSwapBuffers()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Quiz")
    glutDisplayFunc(display)

    init()
    glutSpecialFunc(get_user_special_keyboard_input)
    glutMouseFunc(get_mouse_user_input)
    glutKeyboardFunc(get_user_keyboard_func)
    glutMainLoop()


main()
